<?php
/**
 * Created by PhpStorm.
 * User: oberdanbrito
 * Date: 30/08/2018
 * Time: 08:19
 */

require_once('config.php');
require_once('Backup.php');

$bck = new Backup("host=" . HOST . " port=" . PORT . " dbname=" . DBNAME . " user=" . USER . " password=" . PASSWORD);

$bck->Destino = DESTINO;
$bck->VerificaDestino();

foreach ($bck->Esquemas as $key => $value) {
    echo $value['schema_name'] . "\n";
}

$bck->CopiarEsquema('config');